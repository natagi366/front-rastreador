import { API_KEY } from "./pages/API_KEY";
import { Loader} from 'google-maps';
import { mapOptions } from "../config/mapOptions.js";

const loader = new Loader(API_KEY);

export default {
    render : (aCoordenada) => {
        loader.load().then(function (google) {
            mapOptions.center = {
                lat: parseFloat(aCoordenada[0].latitude),
                lng: parseFloat(aCoordenada[0].longitude)
            };

            const map = new google.maps.Map(document.getElementById("map"), mapOptions);

            for (let oCoordenada of aCoordenada) {
                new google.maps.Marker({
                    position: { lat: parseFloat(oCoordenada.latitude), lng: parseFloat(oCoordenada.longitude) },
                    label: oCoordenada.nome,
                    map
                });
            }
        });
    }
}