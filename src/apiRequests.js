import Vue from 'vue';
import VueResource from 'vue-resource';
import { API } from "../config/api.js";

Vue.use(VueResource);

export default {
    get : (sUri, aParametros = [], fnCallback) => {
        let sParametros = "";

        if (aParametros.length > 0) {
            sParametros = "/"+aParametros.join("/");
        }

        return Vue.http.get(API.url + sUri + sParametros, {
            headers: {
                'Authorization': sessionStorage.getItem("sToken")
            }
        }).then(response => response.json()).then(fnCallback)
        .catch(response => response.json()).then((error) => {
            if (error.messages != undefined) {
                alert(error.messages.error);
            } else {
                alert(error.message);
            }      
        });
    },
    post : (sUri, oParametros, fnCallback) => {
        Vue.http.post(API.url + sUri, oParametros,
        {
            emulateJSON: true,
            headers: {
                'Authorization': sessionStorage.getItem("sToken")
            }
        }).then(response => response.json()).then(fnCallback)
        .catch(response => response.json()).then((error) => {
            if (error.messages != undefined) {
                alert(error.messages.error);
            } else {
                alert(error.message);
            }      
        });
    },
    delete : (sUri, aParametros, fnCallback) => {        
        let sParametros = "/"+aParametros.join("/");

        return Vue.http.delete(API.url + sUri + sParametros, {
            headers: {
                'Authorization': sessionStorage.getItem("sToken")
            }
        }).then(response => response.json()).then(fnCallback)
        .catch(response => response.json()).then((error) => {
            if (error.messages != undefined) {
                alert(error.messages.error);
            } else {
                alert(error.message);
            }      
        });
    }
}