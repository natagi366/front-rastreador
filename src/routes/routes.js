import NavbarLayout from "@/pages/Layout/NavbarLayout.vue";
import Localizacao from "@/pages/Localizacao.vue";
import Login from "@/pages/Login.vue";
import MeusDispositivos from "@/pages/MeusDispositivos.vue";
import PesquisaLocalizacao from "@/pages/PesquisaLocalizacao.vue";

const routes = [
  {
    path: "*",
    component: Login
  },
  {
    path: "/",
    name: "Login",
    component: Login
  },
  {
    path: "/app",
    component: NavbarLayout,
    redirect: "/app/localizacao",
    children: [
      {
        path: "localizacao",
        name: "Localização",
        meta: {
          hideFooter: true,
          requiresAuth: true
        },
        component: Localizacao,
        icon : "location_on"
      },
      {
        path: "pesquisaLocalizacao",
        name: "Pesquisar Localização",
        meta: {
          hideFooter: false,
          requiresAuth: true
        },
        component: PesquisaLocalizacao,
        icon : "date_range"
      },
      {
        path: "meusDispositivos",
        name: "Meus Dispositivos",
        meta: {
          requiresAuth: true
        },
        component: MeusDispositivos,
        icon : "settings_remote"
      }
    ]
  }
];

export default routes;
